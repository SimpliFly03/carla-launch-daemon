#!/bin/bash
function ignore_ctrlc() {
        echo "Terminated!"
}

trap ignore_ctrlc SIGINT SIGTERM

touch $CARLA_ROOT/RunCarla

sleep 5s
# Put the command which will be running while the simulator is running here.
# When Ctrl+c is pressed the command below will be terminated and
# the daemon will be commanded to terminate Carla.
ros2 launch carla_ros_bridge carla_ros_bridge_with_example_ego_vehicle.launch.py "$@" #Example

sleep 5s

rm $CARLA_ROOT/RunCarla
