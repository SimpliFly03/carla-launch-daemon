#!/usr/bin/python3
import os
import signal
import subprocess
import time
import sys


def signal_handler(dummy1, dummy2):
	global carla_process
	global is_running
	print("Terminating...")
	if os.path.isfile('RunCarla'):
		subprocess.run(['rm', 'RunCarla'])
	if is_running:
		os.killpg(os.getpgid(carla_process.pid), signal.SIGKILL)
		carla_process.wait()
	sys.exit(0)


carla_process = subprocess.Popen(['echo', '"CarlaD"'], preexec_fn=os.setsid)

signal.signal(signal.SIGINT, signal_handler)

if os.path.isfile('RunCarla'):
	subprocess.run(['rm', 'RunCarla'])

is_running = False

while(True):
	if (is_running is True) and (carla_process.poll() is not None):
		is_running = False
		if os.path.isfile('RunCarla'):
			subprocess.run(['rm', 'RunCarla'])
		print("Carla has been terminated externally!")
	if os.path.isfile('RunCarla') and (is_running is False):
		carla_process = subprocess.Popen(
			['./CarlaUE4.sh', '-quality-level=Low', '-RenderOffScreen', '-fps=25'],
			preexec_fn=os.setsid)
		is_running = True
		print("Carla has been launched!")
	elif (os.path.isfile('RunCarla') is False) and (is_running is True):
		os.killpg(os.getpgid(carla_process.pid), signal.SIGKILL)
		carla_process.wait()
		is_running = False
		print("Carla has been terminated!")

	time.sleep(5)
